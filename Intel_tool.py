#IP Intel Tool thedigitalfort 2020
import csv
import pygeoip
import collections
import socket
import sys
import subprocess
import shlex
class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[91m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    ENDC = '\033[0m'

#if len(sys.argv) != 2:
#    sys.stderr.write("Usage: {} FILENAME\n".format(sys.argv[0]))
#    exit()

with open('data.dat','rb') as csvfile:

	c2_reader = csv.reader(csvfile)

        #Initialise the Geocity data cache#
        gi = pygeoip.GeoIP('/usr/local/Projects/GeoLiteCity.dat', pygeoip.MEMORY_CACHE)
	#c2_reader = csv.reader(csvfile, delimiter=',', quotechar='"')
	
        i = 0;
	for row in c2_reader:
		#Get the row of the input csv file#
                c2_ip_var = row
		i = i + 1
		
                #once csv IP line is obtained, get required GeoCity fields#
		full_record = gi.record_by_addr(c2_ip_var[0]) #get whole GeoIP dataset
                
                #Now define a reverse lookup function with error control#
                def lookup(addr):
                    try:
                        return socket.gethostbyaddr(addr)                #c2_ip_var[0])
                    except socket.herror:
                        return None, None, None
                
                #use the reverse lookup function#
                hostName = lookup(c2_ip_var[0])
                #First line of output is the reverse lookup#
                print bcolors.OKBLUE + str(c2_ip_var) + " No. " + str(i) + bcolors.ENDC + " IP Reverse lookup: ", hostName
                #do WHOIS on IP after building CMD
                #cmd="whois -h whois " + str(c2_ip_var[0])
                #proc=subprocess.Popen(shlex.split(cmd),stdout=subprocess.PIPE)
                #whoisORG,err=proc.communicate()
                #print whoisORG
                #Output the GeoCity location result#
                if (full_record == None): 
			print "nope not found"
		else:
			print " ",bcolors.OKGREEN + str(full_record['city']) + bcolors.ENDC, ",  ", str(full_record['country_name'])
		#print "{lat:" + str(out_ip) ############'latitude']) + "," + "lng:" + str(out_ip['longitude']) + "," + "label:" + '"' + str(row['cc_ip'] + '"' + "," + "strain:" + '"' + row['infection'] + '"' + "},")
