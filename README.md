# A python tool to get intel on a large list of IPs
# run python2 Intel_check.py 
# By default takes data.dat as a list of character returned IPs to check.
# 
# 
# Requires pygeoip and /usr/local/Projects/GeoLiteCity.dat database from Maxmind
# Available at https://dev.maxmind.com/geoip/geoip2/geolite2/